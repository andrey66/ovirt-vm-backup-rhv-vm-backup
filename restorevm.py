#!/bin/python
import subprocess
#import logging
import os
import ovirtsdk4 as sdk
import ovirtsdk4.types as types
#import ssl
import ConfigParser
import sys
import time
#from datetime import datetime
import virtbkp_utils
#from urlparse import urlparse
#from httplib import HTTPSConnection
import printf


class restore_vm():
    def __init__(self,conf_file,vmname):
        cfg = ConfigParser.ConfigParser()
        cfg.readfp(open(conf_file))
        self.url = cfg.get('restore', 'url')
        self.user = cfg.get('restore', 'user')
        self.password = cfg.get('restore', 'password')
        self.ca_file = cfg.get('restore', 'ca_file')
        self.storagedomain = cfg.get('restore', 'storage')
        self.bkpvm = cfg.get('restore', 'bkpvm')
        self.bckdir = cfg.get('restore', 'bckdir')
        self.connection = None
        self.VM=vmname
        self.mdatap = False
        self.mdvmname = None
        self.mdcores = None
        self.mdsock = None
        self.mdmem = None
        self.mdtype = None
        self.mdostype= None
        self.mdtz = None
        self.mddb = None
        self.listdisk = []
        self.diskformat = []
        self.diskids = None
        self.diskpath = None
        self.diskiface = []

    def start(self):
        try:
          # Create a connection to the server:
          self.connection = sdk.Connection(
                url=self.url,
                username=self.user,
                password=self.password,
                ca_file=self.ca_file)
          printf.INFO("Se ha conectado correctamente a: %s" % self.url)
        except Exception as ex:
          printf.INFO("Unexpected error: %s" % ex)

    def read_vm_metada(self):
        # we try to read the metada, if the metadata not exist, the restore be limited to disk
        # the disk check if exist isn't part of this segment.
        printf.INFO("Trying to read VM metadata")
        try:
            mdataf = self.bckdir + '/' + self.VM + '/' + self.VM + '.mdata'
            cfg = ConfigParser.ConfigParser()
            cfg.readfp(open(mdataf))
            self.mdvmname = cfg.get('Virtual Machine Metadata', 'vm_name')
            self.mdcores = cfg.get('Virtual Machine Metadata', 'cpu_cores')
            self.mdsock = cfg.get('Virtual Machine Metadata', 'cpu_sockets')
            self.mdmem = cfg.get('Virtual Machine Metadata', 'memory')
            self.mdtype = cfg.get('Virtual Machine Metadata', 'vm_type')
            self.mdostype = cfg.get('Virtual Machine Metadata', 'os_type')
            self.mdtz = cfg.get('Virtual Machine Metadata', 'vm_tz')
            self.mddb = cfg.get('Virtual Machine Metadata', 'bootdisk')
            self.mdnn = cfg.get('Virtual Machine Metadata', 'nnics')

            for disk in self.listdisk:
                self.diskformat.append(str(cfg.get(str(disk).split('.')[0], 'format')))
                self.diskiface.append(str(cfg.get(str(disk).split('.')[0], 'interface')))

            self.mdatap = True
            printf.OK("The metadata was read successful")
        except Exception as ex:
            printf.ERROR("An error was ocurred when read metadata, the restore process continue only with disk.")
            printf.ERROR(str(ex))
            self.mdatap = False


    def search_bkp_disk(self):
        try:
            disks = os.listdir(self.bckdir + self.VM)
            for disk in disks:
                if disk != str(self.VM + ".mdata" ):
                    self.listdisk.append(str(disk))
            return True
        except Exception as ex:
            printf.ERROR("An error was ocurred when read the backup list")
            printf.ERROR(str(ex))
            return False

    def create_floating_disk(self,disk,diskformat):
        try:
            # get info of qcow disk on directory
            disk_format = None
            provisioned_size = None

            utils = virtbkp_utils.virtbkp_utils()
            # Get the reference to the disks service:
            disks_service = self.connection.system_service().disks_service()
            provisioned_size = utils.get_qcow_size(self.bckdir + self.VM + "/" + disk)
            # create floaiting disk

            printf.INFO("Trying to create the disk: " + str(disk))
            disknew = disks_service.add(types.Disk(name=str(disk).split('.')[0],description='Restore Disk', format=diskformat,
                    provisioned_size=provisioned_size ,storage_domains=[types.StorageDomain(name=self.storagedomain)]))

            # Wait till the disk is completely created:
            disk_service = disks_service.disk_service(disknew.id)
            while True:
                time.sleep(5)
                disknew = disk_service.get()
                if disknew.status == types.DiskStatus.OK:
                    printf.OK("The disk was created with serial: " + str(disknew.id))
                    self.diskids = str(disknew.id)
                    break
            return True
        except Exception as ex:
            printf.ERROR("An error was ocurred creating Floating disk")
            printf.ERROR(str(ex))
            return False

    def get_id_vm(self, vmname):
        vm_service = self.connection.service("vms")
        vms = vm_service.list()
        for vm in vms:
            if vm.name == vmname:
                return vm.id

    def get_logical_disk(self, diskid):
        cmd = "ls -C1 /dev/* | egrep 'sd[a-z]$|vd[a-z]$'"
        listdev = subprocess.check_output(cmd, shell=True).replace("\n",",").replace("\n",",").split(",")
        control = False
        for path in listdev:
            if path != "":
                cmd = "udevadm info --name=" + str(path) + " | grep '^E: ID_SCSI_SERIAL' | awk -F= '{print$2}'"
                serial = subprocess.check_output(cmd, shell=True).replace("\n","")
                if str(serial) == str(diskid):
                    printf.OK("Disk with serial: " + str(diskid) + " found in path " + str(path))
                    control = True
                    return path

        if  control is False:
            printf.ERROR("Disk with serial " + str(diskid) + " not found")
            return None

    def copy_binary_disk(self,disk,diskpath):
        try:
            dirpath = str(self.bckdir)+ "/" + self.VM + "/" + str(disk)
            printf.INFO("Starting copy binary data from " + dirpath + " to disk " + diskpath)
            cmd = "qemu-img convert  " + dirpath + " -O raw " + diskpath
            output = subprocess.check_call(cmd,shell=True)
            if output == 0:
                printf.OK("Binary copy OK for disk: " + str(disk))
            else:
                printf.ERROR("Binary copy FAILED for disk: " + str(disk))
                return False

            return True
        except Exception as ex:
            printf.ERROR("An error was ocurred with the binary copy")
            printf.ERROR(str(ex))
            return False

    def deatt_floating_disk(self,vmid,iddisk, onlydetach):
        try:
            printf.INFO("Starting detach Disk: " + str(iddisk))
            vms_service = self.connection.system_service().vms_service()
            # Locate the service that manages the virtual machine:
            vm_service = vms_service.vm_service(vmid)
            disk_attachments_service = vm_service.disk_attachments_service()
            disk_attachment_service = disk_attachments_service.attachment_service(iddisk)

            time.sleep(5)

            # Deactivate the disk attachment
            disk_attachment_service.update(types.DiskAttachment(active=False))

            # Wait till the disk attachment not active:
            while True:
                time.sleep(5)
                disk_attachment = disk_attachment_service.get()
                if disk_attachment.active is False:
                    break

            time.sleep(3)
            # verify if is only detach the disk or delete.
            if onlydetach is True:
                if disk_attachment_service.remove(detach_only=True) is None:
                    printf.OK("The disk with serial: " + str(iddisk) + " was deattached from VM: " + vmid)
                    return True
                else:
                    printf.ERROR("Failed deattach the disk with serial: " + str(iddisk) + " from VM: " + vmid)
                    return False
            elif onlydetach is False:
                if disk_attachment_service.remove(detach_only=False) is None:
                    printf.OK("The disk with serial: " + str(iddisk) + " was deleted from system")
                    return True
                else:
                    printf.ERROR("Failed to delete the disk with serial: " + str(iddisk) + " from system")
                    return False
            return True

        except Exception as ex:
            if onlydetach is True:
                printf.ERROR("An error was ocurred with de-attaching process")
            elif onlydetach is False:
                printf.ERROR("An error was ocurred with delete process, please delete manually")
            printf.ERROR(str(ex))
            return False

    def del_disk(self,iddisk):
        print
        try:
            printf.INFO("Trying to remove the disk: " + str(iddisk))
            disks_service = self.connection.system_service().disks_service()
            disk = disks_service.disk_service(iddisk)
            if disk.remove() is None:
                printf.OK("The disk with id: " + str(iddisk) + " was removed")

        except Exception as ex:
            printf.ERROR("An error was ocurred when try delete the disk: " + str(iddisk) + " please delete manually")
            printf.ERROR(str(ex))
            return False

    def att_floating_disk(self,vmdest,disk,isboot,ctrlf,diskiface):
    # search vmproxy id
        try:
            self.diskpath = None
            vms_service = self.connection.system_service().vms_service()
            disk_attachments_service = vms_service.vm_service(vmdest).disk_attachments_service()

            printf.INFO("Trying to attach Disk: " + str(disk)+ " to VM: " + str(vmdest))
            disk_attachment = disk_attachments_service.add(
                types.DiskAttachment(
                    interface=diskiface,
                    bootable=isboot,
                    active=True,
                    disk=types.Disk(id=disk),
                    name=str(disk)
                )
            )

            # Wait till the disk is OK:
            disks_service = self.connection.system_service().disks_service()
            disk_service = disks_service.disk_service(disk_attachment.disk.id)
            while True:
                time.sleep(5)
                diskst = disk_service.get()
                if diskst.status == types.DiskStatus.OK:
                    printf.OK("Disk sucessfull present to VM: " + str(vmdest))
                    break

            if ctrlf:
                printf.INFO("Search internal Disk with serial: " + str(disk_attachment.disk.id))
                ppath = self.get_logical_disk(str(disk_attachment.disk.id))
                if ppath != None:
                    self.diskpath= ppath
                    printf.OK("Disk attached sucesfull and found on VM with path: " + str(ppath))
                else:
                    printf.ERROR("Disk with serial: " + str(disk_attachment.disk.id) + " no found on VM. Detaching disk" )
                    self.deatt_floating_disk(vmdest,disk)
                    return False

            return True

        except Exception as ex:
            printf.ERROR("An error was ocurred with the attach process")
            printf.ERROR(str(ex))

    def create_nic(self,vmid,vnicname):
        system_service = self.connection.system_service()
        vms_service = system_service.vms_service()
        nics_service = vms_service.vm_service(vmid).nics_service()
        nics_service.add(
            types.Nic(
                name=vnicname,
                vnic_profile=types.VnicProfile(
                id=None, ),),)

    def create_vm(self):
        try:
            printf.INFO("Validate if the VM exist.")

            vms_service = self.connection.system_service().vms_service()
            vms = vms_service.list(search='name=' + self.VM, case_sensitive=False)
            if int(len(vms)) is not 0:
                self.VM = self.VM + "RES"
                printf.INFO("The VM name exists, the new name is: " + str(self.VM))

            printf.INFO("Creating the new virtual machine")
            svc_path = "vms/" + str(self.get_id_vm(self.bkpvm))
            vms_service = self.connection.service(svc_path)
            vm = vms_service.get()
            vms_service = self.connection.system_service().vms_service()
            vmn = vms_service.add(types.Vm(name=self.VM, cluster=types.Cluster(id=str(vm.cluster.id)),
                                cpu=types.Cpu(topology=types.CpuTopology(cores=int(self.mdcores),sockets=int(self.mdsock))),memory=int(self.mdmem),
                                type=types.VmType(self.mdtype),time_zone=types.TimeZone(name=self.mdtz),
                                os=types.OperatingSystem(type=self.mdostype),
                                high_availability=types.HighAvailability(enabled=True),
                                 template=types.Template(name='Blank'),))
            if vmn.id is not None:
                printf.OK("The new virtual machine was created with id: " + str(vmn.id) )
                return vmn.id

        except Exception as ex:
            printf.ERROR("An error was ocurred with the VM creating")
            printf.ERROR(str(ex))

    def get_vm_status(self,vmid):
        vms_service = self.connection.system_service().vms_service()
        # Locate the service that manages the virtual machine:
        vm_service = vms_service.vm_service(vmid)
        while True:
                time.sleep(2)
                vmstatus = vm_service.get()
                if vmstatus.status == types.VmStatus.UP:
                    printf.OK("The VM is OK")
                    break

    def main(self):
        self.start()
        if self.search_bkp_disk():
            self.read_vm_metada()

        contd = 0
        idsvec=[]
        idbkpvm = self.get_id_vm(self.bkpvm)
        printf.INFO("The VMID for VM Apliance is: " + str(idbkpvm))
        for disk in self.listdisk:
            if self.mdatap:
                if str(self.diskformat[contd]) == 'cow':
                    disk_format = types.DiskFormat.COW
                elif str(self.diskformat[contd]) == 'raw':
                    disk_format = types.DiskFormat.RAW
            else:
                disk_format = types.DiskFormat.COW
                printf.INFO("Metadata is not present, asuming disk format to cow")

            if self.create_floating_disk(disk, disk_format):
                if self.att_floating_disk(idbkpvm,self.diskids,False,True,types.DiskInterface.VIRTIO_SCSI):
                    if self.copy_binary_disk(disk,self.diskpath):
                        # Control for avoid vm paused.
                        self.get_vm_status(idbkpvm)
                        if self.deatt_floating_disk(idbkpvm,self.diskids,True) is not True:
                            printf.INFO("Failed to deattach disks, please detach disk manually and create the new VM.")
                        else:
                            idsvec.append(str(self.diskids))
                    else:
                        # deactivate and delete
                        if self.deatt_floating_disk(idbkpvm,self.diskids,False) is not True:
                            return False
                else:
                    #delete blank disk.
                    if self.del_disk(self.diskids) is False:
                        printf.ERROR("Please delete disk manually")
                        return False
            contd+=1

        if self.mdatap:
            # create vm
            vmnid=self.create_vm()
            contd = 0

            for disk in self.listdisk:

                if self.diskiface[contd] == 'virtio':
                    diskiface = types.DiskInterface.VIRTIO
                elif self.diskiface[contd] == 'ide':
                    diskiface = types.DiskInterface.IDE
                elif self.diskiface[contd] == 'virtio_scsi':
                    diskiface = types.DiskInterface.VIRTIO_SCSI

                if str(disk).split('.')[0] == self.mddb:
                    self.att_floating_disk(vmnid,idsvec[contd],True,False,diskiface)
                else:
                    self.att_floating_disk(vmnid, idsvec[contd], False,False,diskiface)
                contd+=1

            contd = 0

            for nic in range(int(self.mdnn)):
                self.create_nic(vmnid,"nic" + str(contd))
                contd+=1

        else:
            # metada not set finish
            printf.INFO("Metadata not read, please create VM manually.")

r = restore_vm(sys.argv[1],sys.argv[2])
r.main()